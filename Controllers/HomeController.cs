﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HeroesM.Models;
using HeroesM.MyTools;

namespace HeroesM.Controllers
{
    public class HomeController : Controller
    {
        private HeroesContext db;

        public HomeController()
        {
            db = new HeroesContext();
        }

        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                ViewBag.Units = db.Units.AsEnumerable();
            }
            catch (Exception e)
            {
                return new HtmlErrorMessage(e.Message);
            }

            return View();
        }

        [HttpGet]
        public ActionResult CreateUnit()
        {
            return View();
        }

        [HttpGet]
        [ActionName("UpdateUnit")]
        public ActionResult CreateUnit(int id)
        {
            try
            {
                Unit selectedUnit = db.Units.FirstOrDefault(item => item.UnitID == id);

                if (selectedUnit != null)
                {
                    return View("CreateUnit", selectedUnit);
                }
            }
            catch (Exception e)
            {
                return new HtmlErrorMessage(e.Message);
            }

            return new HtmlErrorMessage("Юнит не найден!");
        }

        [HttpPost]
        public ActionResult CreateUnit(Unit unit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Units.Add(unit);
                    db.SaveChanges();

                    ViewBag.Units = db.Units.AsEnumerable();

                    return View("Index");
                }
            }
            catch (Exception e)
            {
                return new HtmlErrorMessage(e.Message);
            }

            return View();
        }

        [HttpPost]
        public ActionResult UpdateUnit(Unit unit)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Unit updateUnit = db.Units.FirstOrDefault(item => item.UnitID == unit.UnitID);

                    if (updateUnit != null)
                    {
                        updateUnit.Name = unit.Name;
                        updateUnit.Attack = unit.Attack;
                        updateUnit.Defense = unit.Defense;
                        updateUnit.Cost = unit.Cost;

                        db.SaveChanges();

                        ViewBag.Units = db.Units.AsEnumerable();

                        return View("Index");
                    }
                    else
                    {
                        return new HtmlErrorMessage("Обновляемый юнит не найден!");
                    }
                }
            }
            catch (Exception e)
            {
                return new HtmlErrorMessage(e.Message);
            }
            //ибо при возвращении от Валидации не возвращает UnitID
            return View("CreateUnit", unit);
        }

        [HttpPost]
        public ActionResult DeleteUnit(int? id)
        {
            if (id == null)
            {
                return new HtmlErrorMessage("Введите id удаляемого юнита");
            }

            try
            {
                Unit selectedUnit = db.Units.FirstOrDefault(item => item.UnitID == id);

                if (selectedUnit != null)
                {
                    db.Units.Remove(selectedUnit);
                    db.SaveChanges();

                    ViewBag.Units = db.Units.AsEnumerable();

                    return View("Index");
                }
            }
            catch (Exception e)
            {
                return new HtmlErrorMessage(e.Message);
            }

            return new HtmlErrorMessage("Удаляемый юнит не найден!");
        }

    }
}