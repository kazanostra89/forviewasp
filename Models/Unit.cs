﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace HeroesM.Models
{
    public class Unit
    {
        [Key]
        public int UnitID { get; set; }

        [Required(ErrorMessage = "Требуется значение поля - Имя")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Требуется значение поля - Атака")]
        public int Attack { get; set; }

        [Required(ErrorMessage = "Требуется значение поля - Защита")]
        public int Defense { get; set; }

        [Required(ErrorMessage = "Требуется значение поля - Стоимость")]
        public int Cost { get; set; }
    }
}