﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HeroesM.Models
{
    public class HeroesContext : DbContext
    {
        public HeroesContext()
            : base("HeroesContext")
        {

        }

        public DbSet<Unit> Units { get; set; }
    }
}