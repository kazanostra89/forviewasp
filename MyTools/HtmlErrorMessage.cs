﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HeroesM.MyTools
{
    public class HtmlErrorMessage : ActionResult
    {
        private string message;

        public HtmlErrorMessage(string message)
        {
            this.message = message;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            string htmlCode = "";

            htmlCode += "<!DOCTYPE html>" + "<html lang=\"ru\">";
            htmlCode += "<head> <meta charset=\"UTF-8\"> </head>";
            htmlCode += "<body> <h1>Ошибка при выполнении операции</h1>";
            htmlCode += "<p>";
            htmlCode += message;
            htmlCode += "</p>";
            htmlCode += "</body> </html>";
            
            context.HttpContext.Response.Write(htmlCode);
        }
    }
}